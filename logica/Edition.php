<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/EditionDAO.php";



class Edition{
    private $idEdition;
    private $name;
    private $year;
    private $startDate;
    private $endDate;
    private $internationalCollaboration;
    private $numberOfKeynotes;
    private $conexion;
    private $editionDAO;
    /**
     * @return mixed
     */
    public function getIdEdition()
    {
        return $this->idEdition;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @return mixed
     */
    public function getInternationalCollaboration()
    {
        return $this->internationalCollaboration;
    }

    /**
     * @return mixed
     */
    public function getNumberOfKeynotes()
    {
        return $this->numberOfKeynotes;
    }

  
    public function EditionDAO($idEdition="", $name="",$year="",$startDate="",$endDate="",$internationalCollaboration="",$numberOfKeynotes=""){
        $this -> idEdition = $idEdition;
        $this -> name = $name;
        $this -> year = $year;
        $this -> startDate = $startDate;
        $this -> endDate = $endDate;
        $this -> internationalCollaboration = $internationalCollaboration;
        $this -> numberOfKeynotes = $numberOfKeynotes;
        $this->conexion=new Conexion();
        $this->editionDAO=new Edition($idEdition, $name,$year,$startDate,$endDate,$internationalCollaboration,$numberOfKeynotes);
    
    }
    
    public function consultarIDEdicion(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> editionDAO -> consultarIDEdicion());
        $resultado = $this -> conexion -> extraer();
        $this -> idEdition = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> editionDAO -> consultarTodos());
        $ediciones = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($ediciones, new Edition($resultado[0], $resultado[1],$resultado[2],$resultado[3],$resultado[4],$resultado[5],$resultado[6]));
        }
        $this -> conexion -> cerrar();
        return $ediciones;
    
    
    }
    
    public function consultarEdicion(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> editionDAO -> consultarEdicion());
        $resultado = $this -> conexion -> extraer();
        $this -> name = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    
    
    
    
}







?>