<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/ TopicDAO.php";

class Topic
{

    private $idTopic;

    private $name;

    private $conexion;

    private $topicDAO;

    /**
     *
     * @return mixed
     */
    public function getIdTopic()
    {
        return $this->idTopic;
    }

    /**
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function Topic($idTopic = "", $name = "")
    {
        $this->idTopic = $idTopic;
        $this->name = $name;
        $this->conexion = new Conexion();
        $this->topicDAO = new TopicDAO($idTopic, $name);
    }

    public function consultarTema()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->topicDAO->consultarTema());
        $resultado = $this->conexion->extraer();
        $this->name = $resultado[0];
        $this->conexion->cerrar();
    }

    public function consultarTodos()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->topicDAO->consultarTodos());
        $temas = array();
        while (($resultado = $this->conexion->extraer()) != null) {
            array_push($temas, new Topic($resultado[0], $resultado[1]));
        }
        $this->conexion->cerrar();
        return $temas;
    }

    public function consultarIDTema()
    {
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->topicDAO->consultarIDTema());
        $this->idTopic=$resultado[0];
        $this->conexion->cerrar();
    }
}

?>