<?php

require_once "persistencia/Conexion.php";
require_once "persistencia/EditionTopicDAO.php";

class EditionTopic{
    
    private $idEditionTopic;
    private $accepted;
    private $rejected;
    private $edition;
    private $topic;
    private $conexion;
    private $EditionTopicDAO;
    /**
     * @return mixed
     */
    public function getIdEditionTopic()
    {
        return $this->idEditionTopic;
    }

    /**
     * @return mixed
     */
    public function getAccepted()
    {
        return $this->accepted;
    }

    /**
     * @return mixed
     */
    public function getRejected()
    {
        return $this->rejected;
    }

    /**
     * @return mixed
     */
    public function getEdition()
    {
        return $this->edition;
    }

    /**
     * @return mixed
     */
    public function getTopic()
    {
        return $this->topic;
    }

   
  //Constructor
  
    public function EditionTopicDAO($idEditionTopic = "", $accepted = "", $rejected = "", $edition = "", $topic = "")
    {
        $this->idEditionTopic = $idEditionTopic;
        $this->accepted = $accepted;
        $this->rejected = $rejected;
        $this->edition = $edition;
        $this->topic = $topic;
        
        //abriendo la conexión
        $this->conexion = new Conexion();
        
        //enviando datos al DAO 
        $this->EditionTopicDAO= new EditionTopicDAO($idEditionTopic , $accepted , $rejected, $edition, $topic);
    }
    
    
    public function consultarTotalSubmission(){
        
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->EditionTopicDAO->consultarTotalSubmission());
       //la variable resultado puede ser un array depende la consulta o un dato unicamente con en este caso
       // $resultado=$this->conexion->extraer();
       //para mantener el formato
        return $this->conexion->extraer()[0];
        
      
    }
    
    
    
    public function consultarAcceptedRejectedPapers(){
        $this-> conexion->abrir();
        $this->conexion->ejecutar($this->EditionTopicDAO->consultarAcceptedRejectedPapers());
        $submissions=array();
        while(($resultado=$this->conexion->extraer())!=null){
         //agrega los resultados de la consulta a la variable resultado hasta que esta sea
         //diferente de 0, ingresará 2 valores, los accepted y rejected
            array_push($submissions,$resultado);
            
        }
        $this -> conexion -> cerrar();
        return $submissions;
      
    }
    
    
    public function consultarPapersbyTopic(){
        $this-> conexion->abrir();
        $this->conexion->ejecutar($this->EditionTopicDAO->consultarPapersbyTopic());
        $papersbytopic=array();
        
        while(($resultado=$this->conexion->extraer())!=null){
            
            //[0] sera el tema [1] accepted [2]rejected
        array_push($papersbytopic,$resultado);    
            
        }
        
       return $papersbytopic;
        
              
    }
    
    
    
    
    
   
    }

    
    
?>