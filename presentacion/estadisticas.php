<?php
$EditionTopic=new EditionTopic();
$totalSubmission= $EditionTopic->consultarTotalSubmission();
$acceptedRejectedPapers=$EditionTopic->consultarAcceptedRejectedPapers();
$paperbyTopic=$EditionTopic->consultarPapersbyTopic();

echo "entro a estadisticas";
?>

<script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    var dataPapers = google.visualization.arrayToDataTable([
        ["Accepted", "Rejected"],
        <?php 
        foreach ($acceptedRejectedPapers as $ar){
            echo "['" . $ar[0] . "', " . $ar[1] . "],\n";        
        }        
        ?>
    ]);
    
     
    var optionsPapers = {
        title: "Accepted-Rejected Papers",
       // bar: {groupWidth: "95%"},
        legend: { position: "right" },
        is3D: true,
    };
    var chartPapers = new google.visualization.PieChart(document.getElementById("acceptedRejectedPapers"));
    chartPapers.draw(dataPapers, optionsPapers);

}
</script>