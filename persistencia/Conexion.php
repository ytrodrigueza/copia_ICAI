<?php

class Conexion{
    
    private $mysqli;
    private $resultado;
    
    //abre conexion
    public function abrir(){
        $this -> mysqli = new mysqli("localhost", "root", "", "itiud_icai-s");
        $this -> mysqli -> set_charset("utf8");
    }
    
    //cierra conexión
    public function cerrar(){
        $this -> mysqli -> close();
    }
    
    
    //recibe la sentencia sql y la guarda en la variable resultado
    public function ejecutar($sentencia){
        $this -> resultado = $this -> mysqli -> query($sentencia);
    }
    
    
    //al extraer es extrae un array con los datos de acuerdo a la consulta en la funcion ejecutar, en resultado se convierte en array de los datos de la consulta
    public function extraer(){
        return $this -> resultado -> fetch_row();
    }
    
    
}


?>